#!/bin/bash
for filename in `ls public`; do
    if ! git branch --remote | grep --quiet $filename; then
      echo "Removing old branch deployment $filename"
      rm -rf public/$filename
    fi
done
